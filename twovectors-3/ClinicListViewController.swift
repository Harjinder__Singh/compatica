//
//  ClinicListViewController.swift
//  compatica
//
//  Created by Nick Bodnar on 2/4/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class ClinicListViewController: ObjectCollectionViewController<ClinicView>, CancelDelegate {
    required override init() {
        super.init()
        searchBarHidden = false
        headerText = "Select Clinic"
    }
    
    convenience init(main:Bool) {
        self.init()
        itemSize = ObjectViewSizes.smallSize
    }
    
    override func setupDataCallbacks() {
        // TODO: hook this up
//        State.setOnEmployeesUpdated(update)
    }
    
    override func updateData() -> [objectType] {
        let xs = State.getClinics()
        
        var result = [ClinicView]()
        for x in xs {
            if (filter == "" || x.name!.lowercaseString.containsString(filter.lowercaseString)) && Util.clinicDistance(x) < 50 {
                let v = ClinicView(frame: CGRectMake(0, 0, 150, 75))
                v.clinic = x
                result.append(v)
            }
        }
        
        result.sortInPlace() {
            (a, b) in
            return Util.clinicDistance(a.clinic) < Util.clinicDistance(b.clinic)
        }
        
        return result
    }
    
    override func detail(cell: objectType) {
        let add = ClinicDetailViewController()
        add.x = cell.clinic
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

//
//  DetailView.swift
//  compatica
//
//  Created by Nick Bodnar on 1/28/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class DetailView: XibViewBase {
    // TODO: http://stackoverflow.com/questions/3077109/how-to-check-if-uilabel-is-truncated
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var detailView:UIView!
    
    @IBInspectable var labelText:String? {
        get {
            return text.text
        }
        set {
            text.text = newValue
        }
    }
}

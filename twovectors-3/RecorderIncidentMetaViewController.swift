//
//  RecorderIncidentMetaViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 1/7/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class RecorderIncidentViewController: ObjectCollectionListViewController {
    var delegate:IncidentSelectorViewControllerDelegate!
    
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [IncidentListViewController.init()]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()
    
    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    override func tapped(cell: ObjectView) {
        let i = cell as! IncidentView
        delegate.done(i.incident.id)
    }
}

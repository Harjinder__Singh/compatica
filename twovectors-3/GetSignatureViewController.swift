//
//  GetSignatureViewController.swift
//  compatica
//
//  Created by Nick Bodnar on 3/6/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class GetSignatureViewController: DetailViewController {
    var delegate:GetSignatureViewControllerDelegate!
    
    var initialText = UITextView()
    var name = UITextField()
    var time = UIButton()
    
    var signatureType:SignatureType?
    
    override func loadView() {
        super.loadView()
        
        addTextViewDetail(initialText, "", false, 100)
        addTextDetail(name, "Name")
        addTimeDetail(time, "Time")
    }
    
    override func done(sender: UITapGestureRecognizer) {
        delegate.done(self)
    }
    
    override func cancel(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
}
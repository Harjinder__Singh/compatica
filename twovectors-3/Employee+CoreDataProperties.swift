//
//  Employee+CoreDataProperties.swift
//  compatica
//
//  Created by Nick Bodnar on 3/14/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Employee {

    @NSManaged var active: Bool
    @NSManaged var address: String?
    @NSManaged var birth: NSTimeInterval
    @NSManaged var city: String?
    @NSManaged var companyId: Int64
    @NSManaged var contactAddress: String?
    @NSManaged var contactEmail: String?
    @NSManaged var contactName: String?
    @NSManaged var contactPhone: String?
    @NSManaged var contactRelationship: Int64
    @NSManaged var created: NSTimeInterval
    @NSManaged var employeeId: String?
    @NSManaged var hire: NSTimeInterval
    @NSManaged var id: Int64
    @NSManaged var jobTitle: String?
    @NSManaged var male: Bool
    @NSManaged var maritalStatus: Int64
    @NSManaged var media: Int64
    @NSManaged var modified: NSTimeInterval
    @NSManaged var name: String?
    @NSManaged var notes: String?
    @NSManaged var payRate: Double
    @NSManaged var phone: String?
    @NSManaged var roleType: Int32
    @NSManaged var state: String?
    @NSManaged var username: String?
    @NSManaged var zip: String?

}

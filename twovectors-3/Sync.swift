//
//  Sync.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/13/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation

class Sync<T:Syncable> {
    
    func getServerList(callback:((ErrorType?, [JSON]?) -> Void)) {
        callback(nil, [JSON]())
    }
    
    func getClientList() -> [T] {
        return [T]()
    }
    
    func serverNew(obj:JSON) {
        
    }
    
    func clientNew(obj:T) {
        
    }
    
    func update(server:JSON, _ client:T) {
    
    }
    
    func done(success:Bool) {
        
    }
    
    func sync() {
        getServerList() {
            (err, serverList) in
            if err != nil || serverList == nil {
                return self.done(false)
            }
            
            for obj in serverList! {
                var found = false
                for clientObj in self.getClientList() {
                    if clientObj.compare(obj) != .Different {
                        found = true
                        break
                    }
                }
                
                if !found {
                    self.serverNew(obj)
                }
            }
            
            for clientObj in self.getClientList() {
                var found = false
                for obj in serverList! {
                    if clientObj.compare(obj) != .Different {
                        found = true
                        break
                    }
                }
                
                if !found {
                    self.clientNew(clientObj)
                }
            }

            for obj in serverList! {
                for clientObj in self.getClientList() {
                    if clientObj.compare(obj) == .SameObject {
                        self.update(obj, clientObj)
                    }
                }
            }
            
            self.done(true)
        }
    }
    
    func webRequest(path:String, _ obj:T?, _ method:String, callback:((ErrorType?, NSData?) -> Void)) {
        let req = Util.makeRequest(path)

        req.HTTPMethod = method
        req.timeoutInterval = 2.0
        
        if obj != nil {
            do {
                try req.HTTPBody = obj!.toJSON().rawData()
            } catch let error {
                callback(error, nil)
                return
            }
        }
        
        req.setValue("application/json", forHTTPHeaderField: "Content-Type")
        req.setValue("application/json", forHTTPHeaderField: "Accept")
        req.setValue("sessionid=" + State.sessionId! + "; csrftoken=" + State.sessionCSRF, forHTTPHeaderField: "cookie")
        req.setValue(State.sessionCSRF, forHTTPHeaderField: "X-CSRFToken")
        req.setValue(Util.getUrlBase(), forHTTPHeaderField: "Referer")
        
        let semaphore = dispatch_semaphore_create(0)
        Util.instance.getSession().dataTaskWithRequest(req) {
            (resultData, response, var error) -> Void in
            if let r = response as? NSHTTPURLResponse {
                if r.statusCode >= 500 && error == nil {
                    error = NSError(domain: "unknown", code: r.statusCode, userInfo: ["status code":r.statusCode])
                }
            }

            if error != nil {
                callback(error, nil)
                dispatch_semaphore_signal(semaphore)
                return
            }
            
            callback(nil, resultData)
            dispatch_semaphore_signal(semaphore)
        }.resume()
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
    }
}

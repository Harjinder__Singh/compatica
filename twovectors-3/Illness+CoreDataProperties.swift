//
//  Illness+CoreDataProperties.swift
//  compatica
//
//  Created by Nick Bodnar on 3/14/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Illness {

    @NSManaged var id: Int64
    @NSManaged var incident: Int64
    @NSManaged var media: Int64
    @NSManaged var notes: String?
    @NSManaged var severity: Int16
    @NSManaged var type: String?

}

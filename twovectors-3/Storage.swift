//
//  Storage.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/28/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Storage {
    static let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    static let managedContext = appDelegate.managedObjectContext!
    
    static var saveScheduled = false

    static func save() {
//        objc_sync_enter(State.cacheLock)
//        defer{ objc_sync_exit(State.cacheLock) }
//        
//        if ( saveScheduled ) {
//            return
//        }
//        
//        saveScheduled = true
//        
////        dispatch_after(USEC_PER_SEC * 300000, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
//        dispatch_after(USEC_PER_SEC * 300000, dispatch_get_main_queue()) {
            saveInternal()
//        }
    }
    
    static func saveInternal() {
        print("saving")
        objc_sync_enter(State.cacheLock)
        defer{ objc_sync_exit(State.cacheLock) }
        
        State.cache.removeAll()
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        saveScheduled = false
    }
    
    static func getEntities(name:String) -> [NSManagedObject] {
        return getEntities(name, nil)
    }
    
    static func getEntities(name:String, _ predicate:NSPredicate?) -> [NSManagedObject] {
        let fetchRequest = NSFetchRequest(entityName:name)
        
        fetchRequest.predicate = predicate
        
        do {
            return try managedContext.executeFetchRequest(fetchRequest) as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return []
        }
    }
    
    static func getBlankEntity(type:String) -> NSManagedObject {
        let entity = getBlankEntityInNoMOC(type)
        managedContext.insertObject(entity)
        return entity
    }
    
    static func getBlankEntityInNoMOC(type:String) -> NSManagedObject {
        let entity =  NSEntityDescription.entityForName(type,
            inManagedObjectContext:
            managedContext)
        
        return NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext:nil)
    }
    
    static func addEmployee(employee:Employee) {
        if employee.name == nil || employee.name == "" {
            employee.name = "Unknown"
        }
        if employee.id == 0 {
            employee.id = Int64(arc4random_uniform(1000000000)) * -1
        }
        if employee.birth == 0 {
            employee.birth = NSDate().timeIntervalSinceReferenceDate
        }
        if employee.hire == 0 {
            employee.hire = NSDate().timeIntervalSinceReferenceDate
        }
        
        save()
    }
    
    static func addLocation(location:Location) {
        if location.name == nil || location.name == "" {
            location.name = "Unknown"
        }
        if location.id == 0 {
            location.id = Int64(arc4random_uniform(1000000000)) * -1
        }
        
        save()
    }
    
    static func addZone(zone:Zone) {
        if zone.id == 0 {
            zone.id = Int64(arc4random_uniform(1000000000)) * -1
        }
        
        save()
    }
    
    static func addEquipment(equipment:Equipment) {
        if equipment.id == 0 {
            equipment.id = Int64(arc4random_uniform(1000000000)) * -1
        }
        
        save()
    }
    
    static func addClinic(clinic:Clinic) {
        if clinic.id == 0 {
            clinic.id = Int64(arc4random_uniform(1000000000)) * -1
        }
        
        save()
    }
    
    static func addER(er:ER) {
        if er.id == 0 {
            er.id = Int64(arc4random_uniform(1000000000)) * -1
        }
        
        save()
    }
    
    static func addInjury(i: Injury) {
        i.id = Int64(arc4random_uniform(1000000000)) * -1

        save()
    }
    
    static func addIllness(i: Illness) {
        i.id = Int64(arc4random_uniform(1000000000)) * -1
        
        //incidentId:Int64, _ type:String, _ severity:Int16, _ notes:String) {
        
        save()
    }
    
    static func addBodyPart(major:String, _ minor:String) {
        let bodyPart = getBlankEntity("BodyPart") as! BodyPart
        
        bodyPart.id = Int64(arc4random_uniform(1000000000)) * -1
        bodyPart.major = major
        bodyPart.minor = minor
        
        save()
    }
    
    static func addStatement(incidentId:Int64, _ data:NSData, _ type:Int16, _ madeBy:Int64) {
        let media = getBlankEntity("Media") as! Media
        
        media.data = data
        media.id = Int64(arc4random_uniform(1000000000)) * -1
        media.type = type
        
        let statement = getBlankEntity("Statement") as! Statement
        
        statement.incident = incidentId
        statement.id = Int64(arc4random_uniform(1000000000)) * -1
        statement.media = media.id
        statement.madeBy = madeBy
        
        save()
    }
    
    static func addIncident(employeeId:Int64, _ time:NSTimeInterval) -> Incident {
        let incident = getBlankEntity("Incident") as! Incident

        incident.employee = employeeId
        incident.time = time
        incident.id = Int64(arc4random_uniform(1000000000)) * -1
        
        save()
        
        return incident
    }
}
//
//  SyncClinics.swift
//  compatica
//
//  Created by Nick Bodnar on 1/24/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation

class SyncClinics: Sync<Clinic> {    
    override func getServerList(callback: ((ErrorType?, [JSON]?) -> Void)) {
        webRequest("clinic/?json&include=address", nil, "GET") {
            (err, j) in
            if err != nil {
                callback(err, nil)
            } else {
                callback(err, JSON(data: j!).array)
            }
        }
    }
    
    override func getClientList() -> [Clinic] {
        return State.getClinics()
    }
    
    override func serverNew(obj: JSON) {
        let o = Clinic.newObject()
        o.loadJSON(obj)
        State.addClinic(o)
    }

    override func update(server: JSON, _ client: Clinic) {
        client.loadJSON(server)
        
        Storage.save()
    }
    
    override func done(success:Bool) {
        if success {
            SyncERs().sync()
        }
    }
}
//
//  RecorderResponseViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/30/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class RecorderResponseMetaViewController: ObjectCollectionListViewController, RecorderPage, AddDestinationViewControllerDelegate, GetSignatureViewControllerDelegate {
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [FirstAidResponseListViewController.init(), MedicalAttentionResponseListViewController.init(), ResponseDestinationListViewController.init()]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()
    
    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    var needsRefusalAuth = false
    var needsMedicalRelease = false
    var toPage:Int!
    
    override func tapped(cell: ObjectView) {
        if cell is FirstAidResponseView {
            State.currentIncident.firstAidGiven = false
            State.currentIncident.firstAidNotGiven = false
            
            if cell.objectSelected {
                if (cell as! FirstAidResponseView).answer == .Yes {
                    State.currentIncident.firstAidGiven = true
                }
                if (cell as! FirstAidResponseView).answer == .No {
                    State.currentIncident.firstAidNotGiven = true
                }
            }
        }
        
        if cell is MedicalAttentionResponseView {
            needsRefusalAuth = false
            needsMedicalRelease = false

            State.currentIncident.medicalAttentionRefused = false
            State.currentIncident.medicalAttentionRequired = false
            State.currentIncident.medicalAttentionNotRequired = false
            
            if cell.objectSelected {
                if (cell as! MedicalAttentionResponseView).answer == .Yes {
                    State.currentIncident.medicalAttentionRequired = true
                }
                if (cell as! MedicalAttentionResponseView).answer == .No {
                    State.currentIncident.medicalAttentionNotRequired = true
                }
                if (cell as! MedicalAttentionResponseView).answer == .Refused {
                    State.currentIncident.medicalAttentionRefused = true
                    needsRefusalAuth = true
                }
            }
        }
        
        if cell is ResponseDestinationView {
            needsMedicalRelease = false

            State.currentIncident.destinationWork = false
            State.currentIncident.destinationHome = false
            State.currentIncident.destinationClinic = false
            State.currentIncident.destinationER = false
            
            if cell.objectSelected {
                if (cell as! ResponseDestinationView).destination == .Work {
                    State.currentIncident.destinationWork = true
                }
                if (cell as! ResponseDestinationView).destination == .Home {
                    State.currentIncident.destinationHome = true
                }
                if (cell as! ResponseDestinationView).destination == .Clinic {
                    State.currentIncident.destinationClinic = true
                    needsMedicalRelease = true
                }
                if (cell as! ResponseDestinationView).destination == .ER {
                    State.currentIncident.destinationER = true
                    needsMedicalRelease = true
                }
                
                let vc = AddDestinationViewController()
                vc.delegate = self
                vc.clinicOrER = (cell as! ResponseDestinationView).destination
                presentViewController(vc, animated: true, completion: nil)
            }
        }

        updateCollections()
    }
    
    @nonobjc
    func done(vc: GetSignatureViewController) {
        if vc.signatureType == .MedicalRefusal {
            if vc.time.titleLabel != nil && vc.time.titleLabel!.text != nil {
                needsRefusalAuth = false
                State.currentIncident.signatureRefusalDate = Util.dateTimeFormatter.dateFromString(vc.time.titleLabel!.text!)!.timeIntervalSinceReferenceDate
                State.currentIncident.signatureRefusalName = vc.name.text
                
                dispatch_async(dispatch_get_main_queue()) {
                    State.recorderVC.goToPage(self.toPage)
                }
            }
        }
        
        if vc.signatureType == .MedicalInfoRelease {
            if vc.time.titleLabel != nil && vc.time.titleLabel!.text != nil {
                needsMedicalRelease = false
                State.currentIncident.signatureAuthDate = Util.dateTimeFormatter.dateFromString(vc.time.titleLabel!.text!)!.timeIntervalSinceReferenceDate
                State.currentIncident.signatureAuthName = vc.name.text
                
                dispatch_async(dispatch_get_main_queue()) {
                    State.recorderVC.goToPage(self.toPage)
                }
            }
        }
        
        cancel()
    }
    
    @nonobjc
    func done(vc: AddDestinationViewController) {
        cancel()
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func pageStatus() -> RecorderPageStatus {
        return .Good
    }
    
    func leavingPage(toPage: Int) -> Bool {
        self.toPage = toPage

        if needsRefusalAuth {
            let vc = GetSignatureViewController()
            vc.initialText.text = Util.refusalStatement
            vc.signatureType = .MedicalRefusal
            vc.delegate = self
            
            Util.modal(vc, self)
            
            return false
        }
        
        if needsMedicalRelease {
            let vc = GetSignatureViewController()
            vc.initialText.text = Util.medicalReleaseStatement
            vc.signatureType = .MedicalInfoRelease
            vc.delegate = self
            
            Util.modal(vc, self)
            
            return false
        }
        
        return true
    }
}
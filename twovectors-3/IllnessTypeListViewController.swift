//
//  IllnessTypeListViewController.swift
//  compatica
//
//  Created by Nick Bodnar on 1/25/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class IllnessTypeListViewController: ObjectCollectionViewController<IllnessTypeView> {
    
    required override init() {
        super.init()
        headerText = "Select Illness Type"
        itemSize = ObjectViewSizes.smallSize
    }
    
    override func updateData() -> [objectType] {
        let types = ["Sick", "Fever", "Rash", "Hypothermia", "Heat Stroke", "Other"]
        
        var result = [IllnessTypeView]()
        for t in types {
            let i = IllnessTypeView(frame: CGRectMake(0, 0, 150, 75))
            i.illnessType = t
            result.append(i)
        }
        
        return result
    }
}

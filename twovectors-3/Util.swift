//
//  Util.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/26/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import SystemConfiguration
import UIKit

class Util: NSObject, NSURLSessionDelegate {
    static func connected() -> Bool {
//        
//        let reachability = Reachability.reachabilityForInternetConnection
//        var networkStatus = reachability.currentReachabilityStatus
//        return networkStatus != NotReachable
        return true
    }
    
    static func getUrlBase() -> String {
//        return "https://compatica.com/"
                return State.debug ? "http://192.168.0.4:8200/" : "https://compatica.com/"
        //                return State.debug ? "http://172.20.10.2:8200/" : "https://compatica.com/"
    }
    
    static let instance: Util = Util()
    static var refusalStatement = "I refuse Medical Attention."
    static var medicalReleaseStatement = "I authorize the selected Clinic/Hospital to release my medical information to my company."
    
    static var dateFormatter:NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        formatter.timeStyle = .NoStyle
        return formatter
    }()
    
    static var dateTimeFormatter:NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        formatter.timeStyle = .ShortStyle
        return formatter
    }()
    
    static let multiButtonDefaultColor = UIColor.darkGrayColor()
    static let multiButtonSelectedColor = UIColor.blackColor()
    
    static func getImage(mediaId:Int64, _ defaultName:String) -> UIImage? {
        let image = getImage(mediaId) ?? UIImage(named: defaultName)
        if image == nil {
            return nil
        }
        
        return fixOrientation(image!)
    }
    
    static func getImage(mediaId:Int64) -> UIImage? {
        if mediaId != 0 {
            if let m = State.getMedia(mediaId), d = m.data {
                if (m.type == StatementType.Camera.rawValue || m.type == StatementType.Signature.rawValue) {
                    return UIImage(data: d)
                }
            }
        }
        
        return nil
    }
    
    // TODO: my goodness, fix this shit
    static func getCompany() -> Int64? {
        for e in State.getEmployees() {
            if e.companyId > 0 {
                return e.companyId
            }
        }
        
        return nil
    }
    
    static func updateScrollViewSize(sv:UIScrollView) {
        sv.layoutIfNeeded()
        var contentRect = CGRectZero
        for view in sv.subviews {
            if view.frame.width > 7 || view.frame.height > 7 { // handles scrollbars
                contentRect = CGRectUnion(contentRect, view.frame)
            }
        }
        sv.contentSize = contentRect.size
    }
    
    static func dateToTimeInterval(s:String?) -> NSTimeInterval {
        if s == nil {
            return 0
        }
        
        let df = NSDateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSSZZZZZ"
        if let date = df.dateFromString(s!) {
            return date.timeIntervalSince1970
        }
        
        return 0
    }
    
    static func dateToString(t:NSTimeInterval) -> String {
        let df = NSDateFormatter()
        df.dateFormat = "EEE, dd MMM yyyy hh:mm:ss zzz"
        df.timeZone = NSTimeZone(abbreviation: "GMT")
        return df.stringFromDate(NSDate(timeIntervalSinceReferenceDate: t))
    }

    static func modal<T:UIViewController where T:CancelDelegate>(vc:UIViewController, _ owner:T) {
        let modal = Util.storyboard.instantiateViewControllerWithIdentifier("modal") as! ModalViewController
        
        _ = modal.view
        
        modal.addChildViewController(vc)
        modal.container.addSubview(vc.view)
        modal.container.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        vc.view.frame = modal.container.bounds
        vc.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        vc.didMoveToParentViewController(owner)
        
        modal.providesPresentationContextTransitionStyle = true
        modal.definesPresentationContext = true
        modal.modalPresentationStyle = .OverCurrentContext
        
        modal.delegate = owner
        owner.presentViewController(modal, animated: true, completion: nil)
    }
    
    static var storyboard:UIStoryboard {
        get {
            return (UIApplication.sharedApplication().delegate!.window!!.rootViewController?.storyboard)!
        }
    }
    
    static func severityToText(severity:Int16) -> String {
        if severity == Severity.Severe.rawValue {
            return "Severe"
        }
        
        if severity == Severity.FirstAid.rawValue {
            return "First Aid"
        }
        
        return "Record Only"
    }
    
    static func clinicDistance(clinic:Clinic) -> Double {
        let x = clinic
        if ( x.lat != 0 && x.lon != 0 && State.currentIncident != nil && State.currentIncident.getLocation() != nil && State.currentIncident.getLocation()!.lat != 0 && State.currentIncident.getLocation()!.lon != 0) {
            return getDistanceInMiles(x.lat, x.lon, State.currentIncident.getLocation()!.lat, State.currentIncident.getLocation()!.lon)
        }
        
        return -1;
    }
    
    static func erDistance(er:ER) -> Double {
        let x = er
        if ( x.lat != 0 && x.lon != 0 && State.currentIncident != nil && State.currentIncident.getLocation() != nil && State.currentIncident.getLocation()!.lat != 0 && State.currentIncident.getLocation()!.lon != 0) {
            return getDistanceInMiles(x.lat, x.lon, State.currentIncident.getLocation()!.lat, State.currentIncident.getLocation()!.lon)
        }
        
        return -1;
    }
    
    static func getDistanceInMiles(lat1: Double, _ lon1: Double, _ lat2: Double, _ lon2: Double) -> Double {
        
        let l1 = lat1 * M_PI / 180
        let l2 = lat2 * M_PI / 180
        let dlat = (lat2-lat1) * M_PI / 180
        let dlon = (lon2-lon1) * M_PI / 180
        
        let a = sin(dlat/2) * sin(dlat/2) +
            cos(l1) * cos(l2) *
            sin(dlon/2) * sin(dlon/2)
        let c = 2 * atan2(sqrt(a), sqrt(1-a))
        
        return round(6371000 * c / 1609.34) * 1.3
    }

    static func fixOrientation(img:UIImage) -> UIImage {
        if (img.imageOrientation == UIImageOrientation.Up) {
            return img;
        }

        var transform = CGAffineTransformIdentity
        
        if (img.imageOrientation == UIImageOrientation.Down || img.imageOrientation == UIImageOrientation.DownMirrored) {
            transform = CGAffineTransformTranslate(transform, img.size.width, img.size.height)
            transform = CGAffineTransformRotate(transform, CGFloat(M_PI))
        }
        
        if (img.imageOrientation == UIImageOrientation.Left || img.imageOrientation == UIImageOrientation.LeftMirrored) {
            transform = CGAffineTransformTranslate(transform, img.size.width, 0)
            transform = CGAffineTransformRotate(transform, CGFloat(M_PI_2))
        }
        
        if (img.imageOrientation == UIImageOrientation.Right || img.imageOrientation == UIImageOrientation.RightMirrored) {
            transform = CGAffineTransformTranslate(transform, 0, img.size.height);
            transform = CGAffineTransformRotate(transform,  CGFloat(-M_PI_2));
        }
        
        if (img.imageOrientation == UIImageOrientation.UpMirrored || img.imageOrientation == UIImageOrientation.DownMirrored) {
            transform = CGAffineTransformTranslate(transform, img.size.width, 0)
            transform = CGAffineTransformScale(transform, -1, 1)
        }
        
        if (img.imageOrientation == UIImageOrientation.LeftMirrored || img.imageOrientation == UIImageOrientation.RightMirrored) {
            transform = CGAffineTransformTranslate(transform, img.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
        }
        
        let ctx = CGBitmapContextCreate(nil, Int(img.size.width), Int(img.size.height), CGImageGetBitsPerComponent(img.CGImage), 0, CGImageGetColorSpace(img.CGImage), CGImageGetBitmapInfo(img.CGImage).rawValue);
        CGContextConcatCTM(ctx, transform)
        
        if (img.imageOrientation == UIImageOrientation.Left
            || img.imageOrientation == UIImageOrientation.LeftMirrored
            || img.imageOrientation == UIImageOrientation.Right
            || img.imageOrientation == UIImageOrientation.RightMirrored
            ) {
            CGContextDrawImage(ctx, CGRectMake(0,0,img.size.height,img.size.width), img.CGImage)
        } else {
            CGContextDrawImage(ctx, CGRectMake(0,0,img.size.width,img.size.height), img.CGImage)
        }
        
        let cgimg:CGImageRef = CGBitmapContextCreateImage(ctx)!
        
        return UIImage(CGImage: cgimg)
    }
    
    static func makeRequest(path:String) -> NSMutableURLRequest {
        let url = NSURL(string: getUrlBase() + path)
        return NSMutableURLRequest(URL: url!)
    }
    
    func getSession() -> NSURLSession {
        return NSURLSession(configuration: NSURLSessionConfiguration.ephemeralSessionConfiguration(), delegate: self, delegateQueue: nil)
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, willPerformHTTPRedirection response: NSHTTPURLResponse, newRequest request: NSURLRequest, completionHandler: (NSURLRequest!) -> Void) {
        completionHandler(nil)
    }
}
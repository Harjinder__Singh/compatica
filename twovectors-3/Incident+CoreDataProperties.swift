//
//  Incident+CoreDataProperties.swift
//  compatica
//
//  Created by Nick Bodnar on 3/14/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Incident {

    @NSManaged var destinationClinic: Bool
    @NSManaged var destinationClinicId: Int64
    @NSManaged var destinationER: Bool
    @NSManaged var destinationERId: Int64
    @NSManaged var destinationEscortedBy: Int64
    @NSManaged var destinationHome: Bool
    @NSManaged var destinationTime: NSTimeInterval
    @NSManaged var destinationWork: Bool
    @NSManaged var employee: Int64
    @NSManaged var equipment: Int64
    @NSManaged var firstAidGiven: Bool
    @NSManaged var firstAidNotGiven: Bool
    @NSManaged var highestPage: Int16
    @NSManaged var id: Int64
    @NSManaged var location: Int64
    @NSManaged var majorBodyPartIndicator: String?
    @NSManaged var medicalAttentionNotRequired: Bool
    @NSManaged var medicalAttentionRefused: Bool
    @NSManaged var medicalAttentionRequired: Bool
    @NSManaged var signatureAuthDate: NSTimeInterval
    @NSManaged var signatureAuthName: String?
    @NSManaged var signatureRefusalDate: NSTimeInterval
    @NSManaged var signatureRefusalName: String?
    @NSManaged var submitted: Bool
    @NSManaged var time: NSTimeInterval
    @NSManaged var zonex: Int64

}

//
//  ERSelectorViewController.swift
//  compatica
//
//  Created by Nick Bodnar on 2/4/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class ERSelectorViewController: ObjectCollectionListViewController {
    var delegate:ClinicOrERSelectorViewControllerDelegate!
    
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [ERListViewController.init(main: false)]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()
    
    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    override func tapped(cell: ObjectView) {
        let e = cell as! ERView
        delegate.done(clinicOrER: e.er.id)
    }
}

//
//  InjuryTypeSelectorViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 1/17/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class InjuryTypeSelectorViewController: ObjectCollectionListViewController {
    var delegate:InjuryTypeSelectorViewControllerDelegate!
    
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [InjuryTypeListViewController()]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()
    
    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    override func tapped(cell: ObjectView) {
        let i = cell as! InjuryTypeView
        delegate.done(i.injuryType)
    }
}

//
//  RecorderZoneViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/18/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation

import UIKit

class ZoneListViewController: ObjectCollectionViewController<ZoneView>, AddZoneViewControllerDelegate {
    
    required override init() {
        super.init()
        searchBarHidden = false
        addButtonHidden = false
        headerText = "Select Zone"
    }
    
    override func setupDataCallbacks() {
        State.setOnZonesUpdated(update)
    }
    
    override func updateData() -> [objectType] {
        var result = [ZoneView]()
        headerText = ""
        let location = State.currentIncident.location
        
        var noneFound = false
        
        if location != 0 {
            headerText = "Select Zone"
            let zones = State.getZones()
            for z in zones {
                if z.location == location {
                    if z.name!.lowercaseString == "no zone" {
                        noneFound = true
                    }

                    if filter == "" || z.name!.lowercaseString.containsString(filter.lowercaseString) {
                        let zv = ZoneView(frame: CGRectMake(0, 0, 150, 75))
                        zv.zonex = z
                        if State.currentIncident.zonex == z.id {
                            zv.objectSelected = true
                            result = [ZoneView]()
                            result.append(zv)
                            return result
                        }
                        result.append(zv)
                    }
                }
            }
        }
        
        if !noneFound && location != 0 {
            let zone = Zone.newObject()
            zone.name = "No Zone"
            zone.location = location
            State.addZone(zone)
            return updateData()
        }
        
        result.sortInPlace() {
            (a, b) in
            
            if a.zonex.name!.lowercaseString == "no zone" {
                return true
            }
            
            return a.zonex.name?.lowercaseString < b.zonex.name?.lowercaseString
        }
        
        return result
    }
    
    override func detail(cell: objectType) {
        let add = AddZoneViewController()
        add.type = .Edit
        add.zone = cell.zonex
        add.location = State.getLocation(cell.zonex.location)
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    override func add() {
        let add = AddZoneViewController()
        add.type = .Create
        add.location = State.getLocation(State.currentIncident.location)
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    func done(z:Zone) {
        State.addZone(z)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

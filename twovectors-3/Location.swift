//
//  Location.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/1/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import CoreData

class Location: NSManagedObject, Syncable {
    
    func toJSON() -> JSON {
        var j = JSON([:])
        
        j["name"].string = name
        j["description"].string = notes
        j["street_address"].string = address
        j["city"].string = city
        j["state"].string = state
        j["zip_code"].int64Value = Int64(zip ?? "0") ?? 0
        j["company"].int64 = Util.getCompany()
        
        return j
    }
    
    func compare(server: JSON) -> SyncCompare {
        let s = Storage.getBlankEntityInNoMOC("Location") as! Location
        s.loadJSON(server)
        
        let idMatch = id == s.id
        let nameMatch = name == s.name
        if idMatch || nameMatch {
            if idMatch && nameMatch && notes == s.notes && address == s.address && city == s.city && state == s.state && zip == s.zip && modified == s.modified {
                return .Identical
            }
            
            return .SameObject
        }
        
        return .Different
    }
    
    func loadJSON(j: JSON) {
        modified = j["modified_unix"].doubleValue
        
        name = j["name"].stringValue
        id = j["id"].int64Value
        notes = j["description"].string
        
        if j["address"]["street_number"].int64 != nil && j["address"]["street_name"].string != nil {
            address = String(j["address"]["street_number"].int64Value) + " " + j["address"]["street_name"].stringValue
        } else {
            address = nil
        }
        city = j["address"]["city"].string
        state = j["address"]["state"].string
        zip = String(j["address"]["zip_code"].int64 ?? 0) ?? "0"
        lat = j["address"]["latitude"].double ?? 0
        lon = j["address"]["longitude"].double ?? 0
    }
    
    static func newObject() -> Location {
        return Storage.getBlankEntity("Location") as! Location
    }
}

//
//  ER+CoreDataProperties.swift
//  compatica
//
//  Created by Nick Bodnar on 3/14/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ER {

    @NSManaged var address: String?
    @NSManaged var city: String?
    @NSManaged var created: NSTimeInterval
    @NSManaged var id: Int64
    @NSManaged var lat: Double
    @NSManaged var lon: Double
    @NSManaged var modified: NSTimeInterval
    @NSManaged var name: String?
    @NSManaged var notes: String?
    @NSManaged var state: String?
    @NSManaged var zip: String?

}

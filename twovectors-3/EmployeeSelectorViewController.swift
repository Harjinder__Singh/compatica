//
//  EmployeeSelectorViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 1/2/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class EmployeeSelectorViewController: ObjectCollectionListViewController {
    var delegate:EmployeeSelectorViewControllerDelegate!
    
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [EmployeeListViewController.init(main: false)]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()
    
    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    override func tapped(cell: ObjectView) {
        let e = cell as! EmployeeView
        delegate.done(e.employee.id)
    }
}

//
//  CustomViewBase.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/4/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class XibViewBase: UIControl {
    //override var layer:CALayer {
    //    let l = super.layer
    //    l.borderWidth=1.0
    //    l.borderColor=UIColor.whiteColor().CGColor
    //    return l
    //}
    
//    var detailViewController:UIViewController!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        xibSetup()
        customizeLayout()
        layoutIfNeeded()
    }
    
    func xibSetup() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let str = NSStringFromClass(self.dynamicType)
        let index = str.startIndex.advancedBy(10)
        var nibName = str.substringFromIndex(index)
        if self is ObjectView {
            nibName = "ObjectView"
        }
        let nib = UINib(nibName: nibName, bundle: bundle)
        let objs = nib.instantiateWithOwner(self, options: nil)
        
        let view = objs[0] as! UIView
        
        view.frame = bounds
        view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        addSubview(view)
        
//        if objs.count > 1 && objs[1] is UIViewController {
//            detailViewController = objs[1] as! UIViewController
//        }
    }
    
    func customizeLayout() {
    }
    
//    func getDetailViewController() -> UIViewController {
//        return detailViewController
//    }
}

//
//  LoginViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/23/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController: UIViewController, NSURLSessionDelegate {
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var progress: UIActivityIndicatorView!
    
    @IBAction func callEmergency(sender: UIButton, forEvent event: UIEvent) {
        let url = NSURL(string: "telprompt:911")!
        if !UIApplication.sharedApplication().canOpenURL(url) {
            let alert = UIAlertController(title: "Cannot Use Phone", message: "", preferredStyle: .Alert)
            
            let saveAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            
            alert.addAction(saveAction)
            
            presentViewController(alert, animated: true, completion: nil)
            return
        }
        UIApplication.sharedApplication().openURL(url)
    }
    
    @IBAction func login(sender: UIButton, forEvent event: UIEvent) {
        if (State.debug || usernameText.text?.lengthOfBytesUsingEncoding(Constants.theOnlyEncoding) > 0) {
            progress.startAnimating()
            
            let req = Util.makeRequest("login/")
            
            Util.instance.getSession().dataTaskWithRequest(req) {
                (data, response, error) in
                if error != nil {
                    return print(error)
                }
                
                let headers = JSON((response! as! NSHTTPURLResponse).allHeaderFields)
                
                for (_, value) in headers {
                    if value.string != nil && value.stringValue.containsString("csrftoken=") {
                        let start = value.stringValue.characters.indexOf(Character("="))
                        let end = value.stringValue.characters.indexOf(Character(";"))
                        State.sessionCSRF = value.stringValue.substringToIndex(end!).substringFromIndex(start!.advancedBy(1))
                    }
                }
                
                var data = JSON([:])
                data["username"].string = State.debug ? "wim_user" : self.usernameText.text!
                data["password"].string = State.debug ? "password" : self.passwordText.text!
                
                let req = Util.makeRequest("login/")
                
                req.HTTPMethod = "POST"
                req.timeoutInterval = 2.0
                do {
                    //try req.HTTPBody = data.rawData()
                    
                    //i/f ( State.debug ) {
                        req.HTTPBody = String("username=" + data["username"].stringValue + "&password=" + data["password"].stringValue).dataUsingEncoding(Constants.theOnlyEncoding)
                    //}

                    req.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    req.setValue("application/json", forHTTPHeaderField: "Accept")
                    req.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                    req.setValue("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8", forHTTPHeaderField: "Accept")
                    req.setValue("csrftoken=" + State.sessionCSRF, forHTTPHeaderField: "cookie")
                    req.setValue(State.sessionCSRF, forHTTPHeaderField: "X-CSRFToken")
                    
                    req.setValue(Util.getUrlBase(), forHTTPHeaderField: "Referer")
                    
                    Util.instance.getSession().dataTaskWithRequest(req) {
                        (data, response, error) -> Void in
                        if error != nil {
                            print("Error in post: ")
                            print(error!)
                            return
                        }
                        
                        let headers = JSON((response! as! NSHTTPURLResponse).allHeaderFields)
                        
                        for (_, value) in headers
                        {
                            if value.string != nil && value.stringValue.containsString("csrftoken=") {
                                let s = value.stringValue.substringFromIndex(value.stringValue.rangeOfString("csrftoken=")!.startIndex)
                                let start = s.characters.indexOf(Character("="))
                                let end = s.characters.indexOf(Character(";"))
                                State.sessionCSRF = s.substringToIndex(end!).substringFromIndex(start!.advancedBy(1))
                            }
                            if value.string != nil && value.stringValue.containsString("sessionid=") {
                                let s = value.stringValue.substringFromIndex(value.stringValue.rangeOfString("sessionid=")!.startIndex)
                                let start = s.characters.indexOf(Character("="))
                                let end = s.characters.indexOf(Character(";"))

                                let session = s.substringToIndex(end!).substringFromIndex(start!.advancedBy(1))
                                
                                self.successfulLogin(State.debug ? "wim_user" : self.usernameText.text!, session)
//                                self.successfulLogin("wim_user", session)
                            }
                        }
                    }.resume()
                }
//                catch let error {
//                    print("Error in post: ")
//                    print(error)
//                }
            }.resume()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        progress.stopAnimating()
        
        if State.debug {
            login(UIButton(), forEvent: UIEvent())
        }
        super.viewWillAppear(animated)
    }

    func successfulLogin(user:String, _ token: String) {
        progress.stopAnimating()
        State.sessionUser = user
        State.sessionId = token
    }
    
    func clearToken() {
        State.sessionUser = ""
        State.sessionId = ""
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
}

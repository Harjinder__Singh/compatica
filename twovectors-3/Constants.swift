//
//  Constants.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/24/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation

struct Constants {
    static let theOnlyEncoding:UInt = NSUTF8StringEncoding
}

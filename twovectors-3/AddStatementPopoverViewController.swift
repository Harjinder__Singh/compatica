//
//  AddStatementPopoverViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/26/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation

import UIKit

class AddStatementPopoverViewController: UIViewController, UIPopoverPresentationControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var _modalPresentationStyle:UIModalPresentationStyle = .Popover
    override var modalPresentationStyle:UIModalPresentationStyle {
        get {
            return _modalPresentationStyle
        }
        set {
            _modalPresentationStyle = newValue
        }
    }
    
    @IBAction func takePhoto(sender: UIButton, forEvent event: UIEvent) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let pc = UIImagePickerController()
            pc.sourceType = UIImagePickerControllerSourceType.Camera
            pc.delegate = self
            
            pc.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.Camera)!
            
            presentViewController(pc, animated: true, completion: nil)
        }
    }
    
    @IBAction func recordAudio(sender: UIButton, forEvent event: UIEvent) {
        
    }
    
    @IBAction func getSignature(sender: UIButton, forEvent event: UIEvent) {
        
    }
    
    override func loadView() {
        super.loadView()
        modalPresentationStyle = .Popover
        popoverPresentationController?.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationStyle = .Popover
        popoverPresentationController?.delegate = self
    }
    
    func prepareForPopoverPresentation(popoverPresentationController: UIPopoverPresentationController) {
        
    }
    
    func popoverPresentationControllerShouldDismissPopover(popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        
    }
    
    func popoverPresentationController(popoverPresentationController: UIPopoverPresentationController, willRepositionPopoverToRect rect: UnsafeMutablePointer<CGRect>, inView view: AutoreleasingUnsafeMutablePointer<UIView?>) {
        
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    
    func imagePickerController(picker:UIImagePickerController, didFinishPickingMediaWithInfo info:[String:AnyObject] ) {
        picker.dismissViewControllerAnimated(true) {
            _ = info["UIImagePickerControllerOriginalImage"] as! UIImage
            //self.imageView.image = image
        }
    }
}
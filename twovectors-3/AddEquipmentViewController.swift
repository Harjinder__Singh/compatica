//
//  AddEquipmentViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/23/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class AddEquipmentViewController: DetailViewController {
    var delegate:AddEquipmentViewControllerDelegate!
    var equipment:Equipment!
    @nonobjc
    var zone:Zone!
    
    var image = UIImageView(image: UIImage(named: "equipment.png"))
    var name = UITextField()
    var notes = UITextField()
    
    override func loadView() {
        super.loadView()
        
        if zone.name != nil {
            addText("Equipment for Zone: " + zone.name!)
        }
        addImageDetail(image, "Photo\n(Tap to Change)")
        addTextDetail(name, "Name")
        addTextDetail(notes, "Description")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type != .Create {
            name.text = equipment.name
            notes.text = equipment.notes
            image.image = Util.getImage(equipment.media)
        }
    }
    
    override func done(sender: UITapGestureRecognizer) {
        if type == ViewType.Create {
            equipment = Equipment.newObject()
        }
        
        equipment.name = name.text
        equipment.zonex = zone.id
        equipment.notes = notes.text
        
        delegate.done(equipment)
    }
    
    override func cancel(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
}

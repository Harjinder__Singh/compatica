//
//  DetailViewController.swift
//  compatica
//
//  Created by Nick Bodnar on 1/28/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class DetailViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var type:ViewType!

    var doneButton: UIButton!
    var cancelButton: UIButton!
    var scroll: UIScrollView!
    
    var killPicker: UIButton!
    var picker: UIDatePicker!
    
    var tempTime:UIButton!
    var tempEditingImage:UIImageView!
    
    var scrollY = CGFloat(60)
    var scrollHeightDiff = CGFloat(100)
    var nextItemY:CGFloat = 0.0
    var toResize = [[UIButton]]()

    override func loadView() {
        super.loadView()
        
        view.backgroundColor = UIColor.whiteColor()
        
        cancelButton = UIButton(frame: CGRect(x: 0, y: 10, width: 100, height: 30))
        cancelButton.titleLabel?.textAlignment = NSTextAlignment.Left
//        cancelButton.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        cancelButton.setTitle("Cancel", forState: .Normal)
        cancelButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        cancelButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "cancel:"))
        view.addSubview(cancelButton)
        
        doneButton = UIButton(frame: CGRect(x: view.bounds.width - 70, y: 10, width: 70, height: 30))
//        doneButton.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        doneButton.setTitle("Done", forState: .Normal)
        doneButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        doneButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "done:"))
        view.addSubview(doneButton)
        
        scroll = UIScrollView(frame: CGRect(x: 10, y: scrollY, width: view.bounds.width - 20, height: view.bounds.height - scrollHeightDiff))
//        scroll.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        view.addSubview(scroll)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    }
    
    override func viewWillAppear(animated: Bool) {
        resize()
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        resize()
        super.viewDidAppear(animated)
    }
    
    func resize() {
        view.layoutIfNeeded()

        cancelButton.frame = CGRect(x: 0, y: 10, width: 100, height: 30)
        doneButton.frame = CGRect(x: view.bounds.width - 70, y: 10, width: 70, height: 30)
        
        for r in toResize {
            var i = 0
            for b in r {
                let x = b.superview!.bounds.width / CGFloat(r.count)
                b.frame = CGRect(x: CGFloat(i) * x + 5, y: 0, width: x - 10, height: b.superview!.bounds.height)
                i++
            }
        }
        
        scroll.frame = CGRect(x: 10, y: scrollY, width: view.bounds.width - 20, height: view.bounds.height - scrollHeightDiff)
        
//        for v in scroll.subviews {
//            if v.frame.width > 7 && v.frame.height > 7 { // handles scrollbars
//                v.frame = CGRect(x: v.frame.origin.x, y: v.frame.origin.y, width: scroll.bounds.width, height: v.frame.height)
//            }
//        }
        
        Util.updateScrollViewSize(scroll)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        let keyboardFrame: CGRect = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.scroll.frame = CGRect(x: 10, y: self.scrollY, width: self.view.bounds.width - 20, height: self.view.bounds.height - keyboardFrame.size.height - self.scrollHeightDiff)
        })
    }
    
    func keyboardWillHide(notification: NSNotification) {
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.scroll.frame = CGRect(x: 10, y: self.scrollY, width: self.view.bounds.width - 20, height: self.view.bounds.height - self.scrollHeightDiff)
        })
    }

    func addImageDetail(v:UIImageView, _ label:String) {
        let d = DetailView(frame: CGRect(x: 0, y: 30, width: 100, height: 50))
        d.detailView.addSubview(v)
        v.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
//        v.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        d.labelText = label
        d.detailView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "takePhoto:"))
        addItem(d, 60)
    }
    
    func addMultiButtonDetail(buttons:[UIButton], _ actions:[Selector]) {
        let d = UIView(frame: CGRect(x: 0, y: 30, width: 100, height: 80))
        
        var i = 0
        for v in buttons {
            v.backgroundColor = Util.multiButtonDefaultColor
            v.setTitleColor(UIColor.orangeColor(), forState: .Normal)

            d.addSubview(v)
            let x = d.bounds.width / CGFloat(buttons.count)
            v.frame = CGRect(x: CGFloat(i) * x + 5, y: 0, width: x - 10, height: d.bounds.height)
//            v.autoresizingMask = [.FlexibleLeftMargin, .FlexibleWidth, .FlexibleHeight]
            v.addGestureRecognizer(UITapGestureRecognizer(target: self, action: actions[i]))
            i++
        }
        
        toResize.append(buttons)
        
        addItem(d, 80)
    }
    
    func addButtonDetail(v:UIButton, _ label:String, _ tapped:Selector) {
        v.setTitleColor(UIColor.blueColor(), forState: .Normal)
        let d = DetailView(frame: CGRect(x: 0, y: 30, width: 100, height: 50))
        d.detailView.addSubview(v)
        v.frame = d.detailView.bounds
        v.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        d.labelText = label
        
        v.addGestureRecognizer(UITapGestureRecognizer(target: self, action: tapped))
        
        addItem(d, 30)
    }
    
    func addTimeDetail(v:UIButton, _ label:String) {
        v.setTitle(Util.dateTimeFormatter.stringFromDate(NSDate()), forState: .Normal)
        addButtonDetail(v, label, "selectTime:")
    }
    
    func addTextDetail(v:UITextField, _ label:String) {
        v.borderStyle = .Bezel
        let d = DetailView(frame: CGRect(x: 0, y: 30, width: 100, height: 50))
        d.detailView.addSubview(v)
        v.frame = d.detailView.bounds
        v.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        d.labelText = label
        addItem(d, 30)
    }
    
    func addTextViewDetail(v:UITextView, _ label:String) {
        addTextViewDetail(v, label, false)
    }
 
    func addTextViewDetail(v:UITextView, _ label:String, _ editable:Bool) {
        addTextViewDetail(v, label, editable, 30)
    }
 
    func addTextViewDetail(v:UITextView, _ label:String, _ editable:Bool, _ height:CGFloat) {
        let d = DetailView(frame: CGRect(x: 0, y: 30, width: 100, height: height))
        d.detailView.addSubview(v)
        v.frame = d.detailView.bounds
        v.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        d.labelText = label
        
        if editable {
            v.editable = true
            
            v.layer.borderColor = UIColor.blackColor().CGColor
            v.layer.borderWidth = 1.0
        } else {
            v.editable = false
        }
        addItem(d, height)
    }

    func addMapView(text:String, _ title:String, _ subTitle:String, _ lat:Double, _ lon:Double, _ includeSelf:Bool) {
        let d = DetailView(frame: CGRect(x: 0, y: 30, width: 100, height: 150))
        d.labelText = text
        let m = MKMapView(frame: d.detailView.bounds)
        m.showsUserLocation = includeSelf
        m.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        m.mapType = .Standard
        m.centerCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        m.setRegion(MKCoordinateRegionMakeWithDistance(m.centerCoordinate, 5000, 5000), animated: true)
        let x = MapPin(m.centerCoordinate, title, subTitle)
        m.addAnnotation(x)
        
        d.detailView.addSubview(m)
        addItem(d, 150)
    }
    
    class MapPin : NSObject, MKAnnotation {
        var coordinate: CLLocationCoordinate2D
        var title: String?
        var subtitle: String?
        
        init(_ coordinate: CLLocationCoordinate2D, _ title: String, _ subtitle: String) {
            self.coordinate = coordinate
            self.title = title
            self.subtitle = subtitle
        }
    }
    
    func addText(text:String) {
        let l = UILabel()
        l.text = text
        addItem(l, 30)
    }
    
    func addSpace() {
        nextItemY += 10
    }
    
    func addItem(v:UIView, _ height:CGFloat) {
        scroll.addSubview(v)
        v.frame = CGRect(x: 0.0, y: nextItemY, width: scroll.bounds.width, height: height)
        // TODO: option 2: resize above with hacky frame size check
        v.autoresizingMask = [.FlexibleWidth]
        nextItemY += height
        
        Util.updateScrollViewSize(scroll)
    }
    
    func clear() {
        for v in scroll.subviews {
            v.removeFromSuperview()
        }
        
        nextItemY = 0.0
        
        Util.updateScrollViewSize(scroll)
    }
    
    func selectTime(sender: UITapGestureRecognizer) {
        if killPicker == nil {
            let h:CGFloat = 150.0
            picker = UIDatePicker(frame: CGRect(x: 0, y: view.frame.height - h, width: view.frame.width, height: h))
            picker.setDate(NSDate(), animated: true)
            picker.datePickerMode = UIDatePickerMode.DateAndTime
            picker.addTarget(self, action: "dateChanged:", forControlEvents: .ValueChanged)
            picker.backgroundColor = UIColor.whiteColor()
            
            killPicker = UIButton(frame: CGRect(x: view.frame.width - 50, y: view.frame.height - h - 30, width: 50, height: 30))
            killPicker.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "killPicker:"))
            killPicker.setTitle("Done", forState: .Normal)
            killPicker.setTitleColor(UIColor.blackColor(), forState: .Normal)
            
            view.addSubview(picker)
            view.addSubview(killPicker)
            
            tempTime = sender.view as! UIButton
            
            view.endEditing(true)
        }
    }
    
    func dateChanged(datePicker: UIDatePicker) {
        tempTime?.setTitle(Util.dateTimeFormatter.stringFromDate(datePicker.date), forState: .Normal)
    }
    
    func killPicker(sender: AnyObject) {
        killPicker.removeFromSuperview()
        picker.removeFromSuperview()
        view.layoutIfNeeded()
        picker = nil
        killPicker = nil
        tempTime = nil
    }

    func takePhoto(sender: UITapGestureRecognizer) {
        tempEditingImage = sender.view!.subviews[0] as! UIImageView
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let pc = UIImagePickerController()
            pc.sourceType = UIImagePickerControllerSourceType.Camera
            pc.delegate = self
            
            pc.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.Camera)!
            
            presentViewController(pc, animated: true, completion: nil)
        } else {
            setEditingImage(UIImage(named: "camera.png")!)
        }
    }
    
    func setEditingImage(var image: UIImage) {
        image = Util.fixOrientation(image)
        tempEditingImage.image = image
        
        let media = Storage.getBlankEntityInNoMOC("Media") as! Media
        media.data = UIImagePNGRepresentation(image)
        media.id = Int64(arc4random_uniform(1000000000)) * -1
        media.type = StatementType.Camera.rawValue
        (self as? AddEmployeeViewController)?.employee?.media = media.id
        (self as? AddLocationViewController)?.location?.media = media.id
        (self as? AddZoneViewController)?.zone?.media = media.id
        (self as? AddEquipmentViewController)?.equipment?.media = media.id
        (self as? AddInjuryViewController)?.injury?.media = media.id
        (self as? AddIllnessViewController)?.illness?.media = media.id
        
        Storage.managedContext.insertObject(media)
    }
    
    func imagePickerController(picker:UIImagePickerController, didFinishPickingMediaWithInfo info:[String:AnyObject] ) {
        if info.keys.contains("UIImagePickerControllerOriginalImage") {
            let image = info["UIImagePickerControllerOriginalImage"] as! UIImage
            setEditingImage(image)
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func done(sender: UITapGestureRecognizer) {
        fatalError("Not Implemented")
    }
    
    func cancel(sender: UITapGestureRecognizer) {
        fatalError("Not Implemented")
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
}

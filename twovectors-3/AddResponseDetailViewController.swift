//
//  AddResponseDetailViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 1/7/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class AddResponseDetailViewController: DetailViewController {
    var delegate:AddResponseDetailViewControllerDelegate!
    
    var details = UITextView()
    var showFirstAidGivenBy = false
    var firstAidGivenBy = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if showFirstAidGivenBy {
            addTextDetail(firstAidGivenBy, "Aid Provided By")
        }

        addTextViewDetail(details, "Details", true)
    }
    
    override func done(sender: UITapGestureRecognizer) {
        delegate.done(self)
    }
    
    override func cancel(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
}

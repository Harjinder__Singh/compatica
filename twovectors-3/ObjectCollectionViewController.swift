//
//  ObjectCollectionViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/17/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class ObjectViewSizes {
    static let normalSize = UIDevice.currentDevice().userInterfaceIdiom == .Pad ? CGSize(width: 180, height: 90) : CGSize(width: 150, height: 75)
    static let smallSize = UIDevice.currentDevice().userInterfaceIdiom == .Pad ? CGSize(width: 150, height: 75) : CGSize(width: 110, height: 55)
}

class ObjectCollectionViewController<T: ObjectView>: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, AbstractObjectCollection, UISearchBarDelegate {
    
    var parentList:ObjectCollectionList!
    
    typealias objectType = T
    
    var objects = [T]()
    var headerText = ""
    var detailBar = true
    var addButtonHidden = true
    var searchBarHidden = true
    var filter:String = ""
    var itemSize = ObjectViewSizes.normalSize
    var headerHeight = 25
    
    var collectionViewController:UICollectionViewController!
    var header:ObjectCollectionHeader!
    let objectReuseIdentifier = "objectCell"
    let objectHeaderReuseIdentifier = "objectHeader"
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        layout.itemSize = itemSize
        collectionViewController = UICollectionViewController.init(collectionViewLayout: layout)
        collectionViewController.collectionView!.delegate = self
        collectionViewController.collectionView!.dataSource = self
        collectionViewController.collectionView!.scrollEnabled = false
        
        header = ObjectCollectionHeader(frame: CGRect(x: 0, y: 0, width: 300, height: headerHeight))
        header.searchBar.delegate = self

        header.addButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "addInternal:"))
        
        view.addSubview(header)
        
        addChildViewController(collectionViewController)
        collectionViewController.didMoveToParentViewController(self)
        collectionViewController.view.frame = CGRect(x: 0, y: headerHeight, width: 300, height: 300)
        view.addSubview(collectionViewController.view)
        
//        view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
//        header.autoresizingMask = [.FlexibleWidth]
//        collectionViewController.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
//        collectionViewController.collectionView!.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        setupDataCallbacks()
    }
    
    func update() {
        dispatch_async(dispatch_get_main_queue()) {
            self.objects = self.updateData()
            self.collectionViewController.collectionView!.reloadData()
            self.parentList.resize()
            // TODO: why is this necessary. this whole architecture is broken
            self.collectionViewController.collectionView!.reloadData()
            self.parentList.resize()
        }
    }
    
    func layout() {
        collectionViewController.collectionView!.reloadData()
        collectionViewController.collectionView!.layoutIfNeeded()
    }
    
    func getHeight() -> CGFloat {
        if headerText == "" && searchBarHidden && addButtonHidden {
            headerHeight = 0
        } else {
            headerHeight = 25
        }
        
        return CGFloat(headerHeight) + collectionViewController.collectionView!.contentSize.height
    }
    
    func setFrame(r:CGRect) {
        header.label.text = headerText
        header.addButton.hidden = addButtonHidden
        header.searchBar.hidden = searchBarHidden
        
        view.frame = r
        
        if headerText == "" && searchBarHidden && addButtonHidden {
            headerHeight = 0
        } else {
            headerHeight = 25
        }
        
        let hh = CGFloat(headerHeight)
        header.frame = CGRect(x: 0, y: 0, width: r.width, height: hh)
        collectionViewController.view.frame = CGRect(x: 0, y: hh, width: r.width, height: r.height - hh)
    }
    
    override func loadView() {
        super.loadView()
        
        collectionViewController.collectionView?.registerClass(ObjectViewCell<T>.self, forCellWithReuseIdentifier: objectReuseIdentifier)
        collectionViewController.collectionView?.backgroundColor = UIColor.clearColor()
        
        update()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return itemSize
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objects.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(objectReuseIdentifier, forIndexPath: indexPath) as! ObjectViewCell<T>
        cell.objectView = objects[indexPath.row]
        cell.objectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "clickedHandler:"))
        cell.objectView.detail.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "showDetailHandler:"))
        
        cell.objectView.frame = cell.bounds
        cell.objectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        for v in cell.subviews {
            v.removeFromSuperview()
        }
        cell.addSubview(cell.objectView)
        return cell
    }
    
    func addInternal(sender: UIGestureRecognizer) {
        add()
    }
    
    func added() {
        altered()
    }
    
    func altered() {
        State.recorder.updatePageStatuses()
    }
    
    func clickedHandler(sender: UIGestureRecognizer) {
        let cell = sender.view as! T
        if !cell.objectSelected {
            deselectAll()
        }
        cell.objectSelected = !cell.objectSelected

        parentList.tapped(cell)
        
        altered()
    }

    func deselectAll() {
        for o in objects {
            o.objectSelected = false
        }
    }
    
    func showDetailHandler(sender: UIGestureRecognizer) {
        detail(sender.view!.superview!.superview!.superview!.superview as! T)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        filter = searchText
        update()
    }
    
    func setParentList(list: ObjectCollectionList) {
        parentList = list
    }
    
    func getParentList() -> ObjectCollectionList {
        return parentList
    }
    
    func setupDataCallbacks() {
    }
    func updateData() -> [objectType] {
        fatalError("Not Implemented")
    }
    func detail(cell: objectType) {
    }
    func add() {
        fatalError("Not Implemented")
    }
}

class ObjectViewCell<T:ObjectView>: UICollectionViewCell {
    var objectView: T!
}

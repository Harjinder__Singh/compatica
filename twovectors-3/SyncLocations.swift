//
//  SyncLocations.swift
//  compatica
//
//  Created by Nick Bodnar on 1/24/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation

class SyncLocations: Sync<Location> {
    override func getServerList(callback: ((ErrorType?, [JSON]?) -> Void)) {
        webRequest("location/?json&include=address", nil, "GET") {
            (err, j) in
            if err != nil {
                callback(err, nil)
            } else {
                callback(err, JSON(data: j!).array)
            }
        }
    }
    
    override func getClientList() -> [Location] {
        return State.getLocations()
    }
    
    override func serverNew(obj: JSON) {
        let o = Location.newObject()
        o.loadJSON(obj)
        State.addLocation(o)
    }
    
    override func clientNew(obj: Location) {
        webRequest("location/create/?json", obj, "POST") {
            error, data in
            
        }
    }
    
    override func update(server: JSON, _ client: Location) {
        if client.modified < server["modified_unix"].doubleValue {
            let oldId = client.id
            client.loadJSON(server)
            
            if server["id"].int64Value != oldId {
                for i in State.getZones() {
                    if i.location == oldId {
                        i.location = server["id"].int64Value
                    }
                }
                for i in State.getIncidents() {
                    if i.location == oldId {
                        i.location = server["id"].int64Value
                    }
                }
            }
            
            Storage.save()
        } else {
            webRequest("location/" + String(client.id) + "/update/?json", client, "POST") {
                error, data in
                
            }
        }
    }
    
    override func done(success:Bool) {
        if success {
            SyncZones().sync()
        }
    }
}

//
//  SyncERs.swift
//  compatica
//
//  Created by Nick Bodnar on 1/24/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation

class SyncERs: Sync<ER> {
    override func getServerList(callback: ((ErrorType?, [JSON]?) -> Void)) {
        webRequest("hospital/?json&include=address", nil, "GET") {
            (err, j) in
            if err != nil {
                callback(err, nil)
            } else {
                callback(err, JSON(data: j!).array)
            }
        }
    }
    
    override func getClientList() -> [ER] {
        return State.getERs()
    }
    
    override func serverNew(obj: JSON) {
        let o = ER.newObject()
        o.loadJSON(obj)
        State.addER(o)
    }
    
    override func update(server: JSON, _ client: ER) {
        client.loadJSON(server)
        
        Storage.save()
    }
    
    override func done(success:Bool) {
        if success {
            SyncIncidents().sync()
        }
    }
}

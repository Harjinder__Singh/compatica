//
//  AddStatementViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/8/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class AddStatementViewController: DetailViewController,
    //AddAudioViewControllerDelegate,
    //AddSignatureViewControllerDelegate,
EmployeeSelectorViewControllerDelegate {
    var delegate:AddStatementViewControllerDelegate!
    var statement:Statement!
    
    var image = UIImageView(image: UIImage(named: "camera.png"))
    var descriptionText = UITextView()
//    var previewView = UIImageView()
//    var containerView = UIView()
    var prompt = UITextView()
    var notes = UITextView()
    var employeeName = UIButton()
//    var recordAudioButton = UIButton()
    var confirmation = UITextField()
    
    var autoEmployee = UIButton()
    var autoSupervisor = UIButton()
    var autoWitness = UIButton()
    
    var data:NSData?
    
    var madeBy:Employee?
    var statementType:StatementType?
    var viewType:ViewType!
    
    override func loadView() {
        super.loadView()

        prompt.text = "The statement must answer some or all of the following:\nPicture of accident scene\nPicture of injury\nTime the incident occurred\nWho was involved?\nDuties being performed\nEquipment being used\nProtective gear being used\nOther witnesses to the incident?"

        employeeName.setTitle("Tap to Select", forState: .Normal)
//        recordAudioButton.setTitle("Click to Record", forState: .Normal)

        autoEmployee.setTitle("Injured/Ill", forState: .Normal)
        autoSupervisor.setTitle("Supervisor", forState: .Normal)
        autoWitness.setTitle("Witness", forState: .Normal)

        addMultiButtonDetail([autoEmployee, autoSupervisor, autoWitness], ["autoEmployee:", "autoSupervisor:", "autoWitness:"])
        addSpace()
        addButtonDetail(employeeName, "Statement By", "selectEmployee:")
        addTextViewDetail(prompt, "Instructions", false, 170)
        addTextViewDetail(notes, "Details", true, 100)
        addImageDetail(image, "Photo\n(Tap to Change)")
//        addButtonDetail(recordAudioButton, "Record Audio", "recordAudio:")
        addTextDetail(confirmation, "Signature")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if viewType != .Create {
            employeeName.setTitle(statement.getMadeByName(), forState: .Normal)
            madeBy = State.getEmployee(statement.madeBy)
            
            if let mediaImage = Util.getImage(statement.media) {
                image.image = mediaImage
//                showPreview()
            }
        } else {
            dispatch_async(dispatch_get_main_queue()) {
                if self.statementType == .Audio {
//                    self.recordAudio(UIButton(), forEvent: UIEvent())
                } else if self.statementType == .Camera {
//                    self.takePhoto(UIButton(), forEvent: UIEvent())
                } else if self.statementType == .Signature {
                    //self.getSignature(UIButton(), forEvent: UIEvent())
                }
            }
        }
        
        colorButtons()
    }
    
    func selectEmployee(sender: AnyObject) {
        let vc = EmployeeSelectorViewController()
        vc.delegate = self
        
        Util.modal(vc, self)
    }
    
    func setEmployee(employeeId: Int64) {
        madeBy = State.getEmployee(employeeId)
        employeeName.setTitle(madeBy?.name ?? "Tap to Select", forState: .Normal)
        colorButtons()
    }
    
    func autoEmployee(sender: AnyObject) {
        setEmployee(State.currentIncident.employee)
    }
    
    func autoSupervisor(sender: AnyObject) {
        let e = State.getEmployee(State.sessionUser)
        setEmployee(e?.id ?? 0)
    }
    
    func autoWitness(sender: AnyObject) {
        selectEmployee(sender)
    }
    
    func colorButtons() {
        autoEmployee.backgroundColor = Util.multiButtonDefaultColor
        autoSupervisor.backgroundColor = Util.multiButtonDefaultColor
        autoWitness.backgroundColor = Util.multiButtonDefaultColor
        
        if madeBy != nil && madeBy!.id != 0 {
            if madeBy!.id == State.currentIncident.employee {
                autoEmployee.backgroundColor = Util.multiButtonSelectedColor
            } else if madeBy!.id == State.getEmployee(State.sessionUser)?.id {
                autoSupervisor.backgroundColor = Util.multiButtonSelectedColor
            } else {
                autoWitness.backgroundColor = Util.multiButtonSelectedColor
            }
        }
    }
    
//    func takePhoto(sender: UIButton, forEvent event: UIEvent) {
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
//            let pc = UIImagePickerController()
//            pc.sourceType = UIImagePickerControllerSourceType.Camera
//            pc.delegate = self
//            
//            pc.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.Camera)!
//            
//            presentViewController(pc, animated: true, completion: nil)
//        } else {
//            previewView.image = UIImage(named: "camera.png")
//            showPreview()
//        }
//    }
    
//    func recordAudio(sender: AnyObject) {
//        let add = Util.storyboard.instantiateViewControllerWithIdentifier("addAudio") as! AddAudioViewController
//        add.delegate = self
//        presentViewController(add, animated: true, completion: nil)
//    }
    
//    func getSignature(sender: UIButton, forEvent event: UIEvent) {
//        let add = Util.storyboard.instantiateViewControllerWithIdentifier("addSignature") as! AddSignatureViewController
//        add.delegate = self
//        presentViewController(add, animated: true, completion: nil)
//    }
    
//    func showPreview() {
//        for v in containerView.subviews {
//            v.hidden = true
//        }
//        previewView.hidden = false
//    }
    
//    func imagePickerController(picker:UIImagePickerController, didFinishPickingMediaWithInfo info:[String:AnyObject] ) {
//        if info.keys.contains("UIImagePickerControllerOriginalImage") {
//            var image = info["UIImagePickerControllerOriginalImage"] as! UIImage
//            image = Util.fixOrientation(image)
//            previewView.image = image
//            
//            data = UIImagePNGRepresentation(image)
//            type = .Camera
//            
//            showPreview()
//        }
//        
//        dismissViewControllerAnimated(true, completion: nil)
//    }
    
    @nonobjc
    func done(employeeId:Int64) {
        setEmployee(employeeId)
        
        cancel()
    }
    
//    @nonobjc // apple is the worst
//    func done(vc:AddAudioViewController) {
//        //showPreview()
//
//        let newImage = UIImage(named: "recording.png")
//        image.image = newImage
//        
//        data = vc.data
//        statementType = .Audio
//
//        dismissViewControllerAnimated(true, completion: nil)
//    }
    
//    @nonobjc // apple is the worst
//    func done(vc:AddSignatureViewController) {
//        showPreview()
//        
//        let image = vc.getImage()
//        if image != nil {
//            previewView.image = image!
//            data = UIImagePNGRepresentation(image!)
//            statementType = .Signature
//        }
//        
//        dismissViewControllerAnimated(true, completion: nil)
    //    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func done(sender: UITapGestureRecognizer) {
        if image.image != nil {
            data = UIImagePNGRepresentation(image.image!)
            statementType = .Camera
        }
        delegate.done(self);
    }
    
    override func cancel(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
}
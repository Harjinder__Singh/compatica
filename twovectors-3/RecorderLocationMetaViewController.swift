//
//  RecorderLocationMetaViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/19/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class RecorderLocationMetaViewController: ObjectCollectionListViewController, RecorderPage {
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [LocationListViewController.init(), ZoneListViewController.init(), EquipmentListViewController.init()]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()

    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    override func tapped(cell: ObjectView) {
        if cell is LocationView {
            if cell.objectSelected {
                State.currentIncident.location = (cell as! LocationView).location.id
            } else {
                State.currentIncident.location = 0
            }
            State.currentIncident.zonex = 0
            State.currentIncident.equipment = 0
        }
        
        if cell is ZoneView {
            if cell.objectSelected {
                State.currentIncident.zonex = (cell as! ZoneView).zonex.id
            } else {
                State.currentIncident.zonex = 0
            }
            State.currentIncident.equipment = 0
        }
        
        if cell is EquipmentView {
            if cell.objectSelected {
                State.currentIncident.equipment = (cell as! EquipmentView).equipment.id
            } else {
                State.currentIncident.equipment = 0
            }
        }
        
        updateCollections()
    }
    
    func pageStatus() -> RecorderPageStatus {
        if State.currentIncident.equipment == 0 {
            if State.recorderVC.viewControllers!.count == 0 || State.recorderVC.viewControllers![0] == self {
                return .Unknown
            }

            return .Bad
        }
        
        return .Good
    }
    
    func leavingPage(toPage: Int) -> Bool {
        return true
    }
}
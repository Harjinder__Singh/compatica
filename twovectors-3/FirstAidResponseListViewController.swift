//
//  FirstAidResponseListViewController
//  twovectors-3
//
//  Created by Nick Bodnar on 12/21/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class FirstAidResponseListViewController: ObjectCollectionViewController<FirstAidResponseView>, AddResponseDetailViewControllerDelegate {
    
    required override init() {
        super.init()
        headerText = "Was First Aid Given?"
    }

    override func updateData() -> [objectType] {
        let responses = [Answer.Yes, Answer.No]
        
        var result = [FirstAidResponseView]()
        for r in responses {
            let rv = FirstAidResponseView(frame: CGRectMake(0, 0, 150, 75))
            rv.answer = r
            if State.currentIncident.firstAidGiven && r == .Yes {
                rv.objectSelected = true
                result = [FirstAidResponseView]()
                result.append(rv)
                return result
            }
            if State.currentIncident.firstAidNotGiven && r == .No {
                rv.objectSelected = true
                result = [FirstAidResponseView]()
                result.append(rv)
                return result
            }
            result.append(rv)
        }
        
        return result
    }
    
    override func detail(cell: objectType) {
        let vc = AddResponseDetailViewController()
        vc.showFirstAidGivenBy = (cell.answer == .Yes)
        vc.delegate = self
        presentViewController(vc, animated: true, completion: nil)
    }
    
    func done(vc: AddResponseDetailViewController) {
        cancel()
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}


//
//  MinorBodyPartListViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 1/1/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class MinorBodyPartListViewController: ObjectCollectionViewController<MinorBodyPartView> {
    
    required override init() {
        super.init()
        headerText = "Select Minor Body Part"
        itemSize = ObjectViewSizes.smallSize
    }
    
    override func updateData() -> [objectType] {
        let bodyParts = State.getBodyParts()
        
        var result = [MinorBodyPartView]()
        for b in bodyParts {
            if State.currentIncident.majorBodyPartIndicator == b.major {
                let bpv = MinorBodyPartView(frame: CGRectMake(0, 0, 150, 75))
                bpv.bodyPart = b
                result.append(bpv)
            }
        }
        
        return result
    }
}

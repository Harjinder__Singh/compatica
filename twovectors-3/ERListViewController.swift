//
//  ERListViewController.swift
//  compatica
//
//  Created by Nick Bodnar on 2/4/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class ERListViewController: ObjectCollectionViewController<ERView>, CancelDelegate {
    required override init() {
        super.init()
        searchBarHidden = false
        headerText = "Select Hospital"
    }
    
    convenience init(main:Bool) {
        self.init()
        itemSize = ObjectViewSizes.smallSize
    }
    
    override func setupDataCallbacks() {
        // TODO: hook this up
        //        State.setOnEmployeesUpdated(update)
    }
    
    override func updateData() -> [objectType] {
        let xs = State.getERs()
        
        var result = [ERView]()
        for x in xs {
            if (filter == "" || x.name!.lowercaseString.containsString(filter.lowercaseString)) && Util.erDistance(x) < 50 {
                let v = ERView(frame: CGRectMake(0, 0, 150, 75))
                v.er = x
                result.append(v)
            }
        }
        
        result.sortInPlace() {
            (a, b) in
            return Util.erDistance(a.er) < Util.erDistance(b.er)
        }

        return result
    }
    
    override func detail(cell: objectType) {
        let add = ERDetailViewController()
        add.x = cell.er
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

//
//  AddLocationViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/23/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class AddLocationViewController: DetailViewController {
    var delegate:AddLocationViewControllerDelegate!
    var location:Location!
    
    var image = UIImageView(image: UIImage(named: "location.png"))
    var name = UITextField()
    var notes = UITextField()
    var address = UITextField()
    var city = UITextField()
    var state = UITextField()
    var zip = UITextField()
    
    override func loadView() {
        super.loadView()
        
        addImageDetail(image, "Photo\n(Tap to Change)")
        addTextDetail(name, "Name")
        addTextDetail(notes, "Description")
        addTextDetail(address, "Address")
        addTextDetail(city, "City")
        addTextDetail(state, "State")
        addTextDetail(zip, "Zip Code")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type != .Create {
            name.text = location.name
            notes.text = location.notes
            image.image = Util.getImage(location.media)
            address.text = location.address
            city.text = location.city
            state.text = location.state
            zip.text = location.zip
        }
    }
    
    override func done(sender: UITapGestureRecognizer) {
        if type == ViewType.Create {
            location = Location.newObject()
        }
        
        location.name = name.text
        location.notes = notes.text
        location.address = address.text
        location.city = city.text
        location.state = state.text
        location.zip = zip.text
        
        delegate.done(location)
    }
    
    override func cancel(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
}

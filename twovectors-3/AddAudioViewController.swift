//
//  AddAudioViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 11/3/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

// largely copied from www.techotopia.com/index.php/Recording_Audio_on_iOS_8_with_AVAudioRecorder_in_Swift

class AddAudioViewController: UIViewController, AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    
    var delegate:AddAudioViewControllerDelegate!
    var audioPlayer: AVAudioPlayer!
    var audioRecorder: AVAudioRecorder!
    
    var data:NSData?
    
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var instructions: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        instructions.layer.borderColor = UIColor.blackColor().CGColor
        instructions.layer.borderWidth = 1.0
        
        playButton.enabled = false
        
        let dirPaths =
        NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
            .UserDomainMask, true)
        let docsDir = dirPaths[0] 
        let soundFilePath = docsDir + "sound.caf"
        let soundFileURL = NSURL(fileURLWithPath: soundFilePath)
        let recordSettings:[String : AnyObject] = [
            AVEncoderAudioQualityKey: AVAudioQuality.High.rawValue,
            AVEncoderBitRateKey: 16,
            AVNumberOfChannelsKey: 2,
            AVSampleRateKey: 44100.0
        ]
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
        
            audioRecorder = try AVAudioRecorder(URL: soundFileURL, settings: recordSettings)
        } catch let error as NSError {
            print("Error in audio \(error), \(error.userInfo)")
            delegate.cancel()
        }
        
        audioRecorder.delegate = self
        audioRecorder.prepareToRecord()
    }
    
    @IBAction func recordAudio(sender: AnyObject) {
        if audioRecorder.recording {
            audioRecorder.stop()
            doneRecording()
        } else {
            playButton.enabled = false
            recordButton.setTitle("Done", forState: .Normal)
            audioRecorder.record()
        }
    }
    
    @IBAction func playOrRestart(sender: AnyObject) {
        if audioPlayer == nil || !audioPlayer.playing {
            if audioRecorder.recording {
                return
            }
            
            do {
                try audioPlayer = AVAudioPlayer(contentsOfURL: audioRecorder.url)
            } catch let error as NSError {
                print("Error in audio \(error), \(error.userInfo)")
                delegate.cancel()
            }
            
            audioPlayer.delegate = self
            
            audioPlayer.play()
            recordButton.enabled = false
            playButton.setTitle("Stop", forState: .Normal)
        } else {
            audioPlayer.stop()
            donePlaying()
        }
    }
    
    func doneRecording() {
        playButton.enabled = true
        recordButton.setTitle("Retry", forState: .Normal)
        
        data = NSData(contentsOfURL: audioRecorder.url)
    }
    
    func donePlaying() {
        recordButton.enabled = true
        playButton.setTitle("Play", forState: .Normal)
    }
    
    @IBAction func done(sender: UIButton, forEvent event: UIEvent) {
        delegate.done(self)
    }
    
    @IBAction func cancel(sender: UIButton, forEvent event: UIEvent) {
        delegate.cancel()
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        donePlaying()
    }
    
    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer, error: NSError?) {
        print("Audio Play Decode Error")
        delegate.cancel()
    }
    
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder, successfully flag: Bool) {
        doneRecording()
    }
    
    func audioRecorderEncodeErrorDidOccur(recorder: AVAudioRecorder, error: NSError?) {
        print("Audio Record Encode Error")
        delegate.cancel()
    }
}

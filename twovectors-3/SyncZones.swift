//
//  SyncZones.swift
//  compatica
//
//  Created by Nick Bodnar on 1/24/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation

class SyncZones: Sync<Zone> {
    override func getServerList(callback: ((ErrorType?, [JSON]?) -> Void)) {
        webRequest("zone/?json&include=address", nil, "GET") {
            (err, j) in
            if err != nil {
                callback(err, nil)
            } else {
                callback(err, JSON(data: j!).array)
            }
        }
    }

    override func getClientList() -> [Zone] {
        return State.getZones()
    }
    
    override func serverNew(obj: JSON) {
        let o = Zone.newObject()
        o.loadJSON(obj)
        
        updatePic(o)
        
        State.addZone(o)
    }
    
    override func clientNew(obj: Zone) {
        if obj.location > 0 {
            webRequest("zone/create/?json", obj, "POST") {
                error, data in
                
            }
        }
    }
    
    override func update(server: JSON, _ client: Zone) {
        if client.modified < server["modified_unix"].doubleValue {
            let oldId = client.id
            client.loadJSON(server)
            
            updatePic(client)
            
            if server["id"].int64Value != oldId {
                for i in State.getEquipments() {
                    if i.zonex == oldId {
                        i.zonex = server["id"].int64Value
                    }
                }
                for i in State.getIncidents() {
                    if i.zonex == oldId {
                        i.zonex = server["id"].int64Value
                    }
                }
            }
            
            Storage.save()
        } else {
            webRequest("zone/" + String(client.id) + "/update/?json", client, "POST") {
                error, data in
                
            }
        }
    }
    
    func updatePic(o:Zone) {
        if let m = State.getMedia(o.media) {
            if m.data != nil {
                return
            } else {
                webRequest("media/" + m.url!, nil, "GET") {
                    (err, data) in
                    m.data = data
                    Storage.save()
                }
            }
        }
    }
    
    override func done(success:Bool) {
        if success {
            SyncEquipments().sync()
        }
    }
}

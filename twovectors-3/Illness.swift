//
//  Illness.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/23/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import CoreData

class Illness: NSManagedObject {
    static func newObject() -> Illness {
        return Storage.getBlankEntity("Illness") as! Illness
    }
}

//
//  IncidentListViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 1/7/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class IncidentListViewController: ObjectCollectionViewController<IncidentView> {
    required override init() {
        super.init()
    }
    
    override func updateData() -> [objectType] {
        let incidents = State.getIncidents()
        
        var result = [IncidentView]()
        for i in incidents {
            if !i.submitted {
                let iv = IncidentView(frame: CGRectMake(0, 0, 150, 75))
                iv.incident = i
                result.append(iv)
            }
        }
        
        return result
    }
}

//
//  RecorderFinishPreviousViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/30/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class RecorderFinishPreviousViewController: UIViewController, RecorderFinishPreviousViewControllerDelegate, IncidentSelectorViewControllerDelegate {
    @IBOutlet weak var unfinishedList: UIView!
    
    var incidentVC:RecorderIncidentViewController!
    
    var shouldRedirectToApp = true
    
    @IBAction func logout(sender: AnyObject) {
        State.sessionUser = ""
        State.sessionId = ""
    }
    
    @IBAction func newButton(sender: UIButton, forEvent event: UIEvent) {
        loadIncident(0)
    }
    
    func loadIncident(id:Int64) {
        State.setCurrentIncident(id)
//        State.recorderVC.goToPage(0)
        for (k, _) in State.recorderVC.namesToViewControllers {
            State.recorderVC.namesToViewControllers[k] = nil
        }
        shouldRedirectToApp = true
        sessionUpdated()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        State.loginVC = Util.storyboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        State.recorder = Util.storyboard.instantiateViewControllerWithIdentifier("recorderContainer") as! RecorderContainer
        State.recorder.delegate = self
        
        State.recorderVC = RecorderViewController()
        
        incidentVC = RecorderIncidentViewController()
        let vc = incidentVC
        vc.delegate = self
        
        addChildViewController(vc)
        vc.view.frame = unfinishedList.bounds
        vc.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        unfinishedList.addSubview(vc.view)
        unfinishedList.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        vc.didMoveToParentViewController(self)
        
        State.setOnSessionUpdated(sessionUpdated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        incidentVC.updateCollections()
        sessionUpdated()
    }
    
    func sessionUpdated() {
        if State.sessionId == nil || State.sessionId!.lengthOfBytesUsingEncoding(Constants.theOnlyEncoding) == 0 {
            showLogin()
        } else {
            if shouldRedirectToApp {
                showApp()
                shouldRedirectToApp = false
            } else {
                showVC(nil)
            }
            State.recorder.updateSupervisorText()
        }
    }
    
    func showLogin() {
        showVC(State.loginVC)
    }
    
    func showApp() {
        showVC(State.recorder)
    }
    
    func showVC(vc:UIViewController?) {
        if presentedViewController == nil {
            if vc != nil {
                presentViewController(vc!, animated: true, completion: nil)
            }
        } else {
            if presentedViewController != vc && presentedViewController != nil {
                dismissViewControllerAnimated(true) {
                    self.sessionUpdated()
                }
            }
        }
    }
    
    func done(incidentId: Int64) {
        loadIncident(incidentId)
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
